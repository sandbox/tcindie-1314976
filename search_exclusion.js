(function ($) {

Drupal.behaviors.search_exclusionFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.search-exclusion-form', context).drupalSetSummary(function (context) {
      var search_exclusion_setting = $('.form-item-search-exclusion-setting input:checked').val();
      
      if (search_exclusion_setting == 1) {
      	return Drupal.t('Excluded from index');
      }
      else {
      	return Drupal.t('Included in index');
      }

    });
  }
};

})(jQuery);